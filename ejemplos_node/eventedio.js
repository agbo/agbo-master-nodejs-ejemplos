var fs = require('fs');

// abrimos un stream de escritura hacia un fichero
var file = fs.createWriteStream('./out.txt');

// el stream lanza eventos 'data', entre otros 
process.stdin.on('data', function(data) {
  	file.write(data);
});
process.stdin.on('end', function() {
  	file.end();
});

// quitamos el pause a stdin
process.stdin.resume(); 

// ahora si vamos a la consola y escribimos debería echarlo al fichero