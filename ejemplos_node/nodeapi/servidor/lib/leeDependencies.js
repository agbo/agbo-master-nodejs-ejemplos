"use strict";

// abrir fichero package.json

var fs = require('fs');

function leeDependencies() {

    return new Promise( function(resolve, reject) {

        fs.readFile('./package.json', function (err, data) {
            if (err) {
                return reject(err); //cb(err);
            }

            try {

                // parsearlo
                var packageJson = JSON.parse(data);

            } catch (e) {

                return reject(e); //cb(e);

            }

            // devolver las dependencias

            //return cb(null, Object.getOwnPropertyNames(packageJson.dependencies));
            return resolve( Object.getOwnPropertyNames(packageJson.dependencies));

        });



    }); // cierre de la promesa





}

module.exports = leeDependencies;