'use strict';

var passport = require('passport');
var LocalAPIKeyStrategy = require('passport-localapikey').Strategy;
var config = require('../local_config');

passport.use(new LocalAPIKeyStrategy(
    function(apikey, done) {
        /*User.findOne({ apikey: apikey }, function (err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            return done(null, user);
        });*/
        process.nextTick(function () {
            if (apikey !== config.apiKey) {
                return done(null, false);
            }
            return done(null, {name: 'nombredeusuario', id: 77});
        });
    }
));

var apiKeyAuth = function(req, res, next) {
    passport.authenticate('localapikey', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (info) {
            console.info('apiKeyAuth info', info);
        }
        if (!user) {
            // opcional: hacer log de accesos no autorizados
            return res.json({ok: false, error: {code: 401, message: 'Unauthorized'}});
        }
        next();
    })(req, res, next);
};

module.exports = apiKeyAuth;
