"use strict";

var express = require('express');
var router = express.Router();

// cargamos nuestra utilidad y lo usamos como si fuera un modelo
var leeDependencies = require('../lib/leeDependencies');

router.get('/', function(req, res, next) {

/*
    // le pido al modelo datos
    leeDependencies(function(err, data){
        if (err) {
            console.log(err);
            return next(err);
        }
        // paso los datos que me da el modelo a la vista dependencies
        res.render('dependencies', { data: data });
    });
*/
    // le pido al modelo datos
    leeDependencies().then(function(data){
        // paso los datos que me da el modelo a la vista dependencies
        res.render('dependencies', { data: data });
    }).catch(function(err) {
        console.log(err);
        return next(err);
    });


});


module.exports = router;