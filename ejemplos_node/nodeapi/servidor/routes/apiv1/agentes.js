"use strict";

var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
//var Agente = require('../models/Agente'); // alternativa
var Agente = mongoose.model('Agente'); //



// devuelve una lista de agentes en JSON
router.get('/', function(req, res) {

    //var criterios = {name:'Jones'};

    // sacar criterios de busqueda de query-string
    // ej. /apiv1/agentes/?name=Jones
    var criterios = {};
    if (req.query.name) {
        criterios.name = req.query.name;
    }

    Agente.lista(criterios, function(err, lista) {
        if (err) {
            console.log(err);
            return res.json({ok:false, error: err});
        }

        res.json({ok:true, data: lista});

    });

});

// crea un agente
// POST /apiv1/agentes
router.post('/', function(req, res, next) {

    var nuevo = req.body;

    // crear un registro de agente
    var agente = new Agente(nuevo); // {name:'Nuevo', age: 18}

    agente.save( function(err, creado) {
        if (err) {
            console.log(err);
            return res.json({ok:false, error: err});
        }

        // devolver una confirmación
        res.json({ok:true, agente: creado});

    });

});




module.exports = router;