"use strict";

var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
//var Agente = require('../models/Agente'); // alternativa
var Agente = mongoose.model('Agente'); //

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

// crea un agente, el mismo en cada post
router.post('/', function(req, res, next) {

    // crear un registro de agente
    var agente = new Agente({name:'Nuevo', age: 18});

    agente.save( function(err, creado) {
        if (err) {
            console.log(err);
            return next(err);
        }

        console.log(creado);

    });

    res.send('respond with a resource');
});



module.exports = router;