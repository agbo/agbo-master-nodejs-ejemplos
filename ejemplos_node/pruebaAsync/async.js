'use strict';

var async = require('async');

// definir un array
var arr = ['uno', 'dos', 'tres'];

// definir un iterador
var iterador = function(elemento, callback) {
    setTimeout(function(){
        if (elemento === 'dos') {
            return callback(null);
        }
        elemento = elemento + '-procesado';
        callback(null, elemento);
        return;
    }, 0);
};

// procesar el array con iterador, y recoger resultados cuando acaben
// https://github.com/caolan/async#concat
async.concat(arr, iterador, function(err, resultados) {
    // mostrar resultados
   console.log(resultados);
});

