// cargar un módulo básico
require('./basico.js');

// exporta una función anónima
var anonima = require('./anonima.js');
anonima();

// exporta una función con nombre
var nombre = require('./nombre.js').nombre;
nombre();

// exporta un objeto anónimo - uno de los más comunes
var objeto_anonimo = require('./objeto_anonimo.js');
objeto_anonimo.salta();

// exporta un objeto con nombre
var objeto_nombre = require('./objeto_nombre.js').objeto;
objeto_nombre.salta();

// exporta un constructor anónimo - otro de los más comunes
var ConstructorAnonimo = require('./ConstructorAnonimo.js');
var construido1 = new ConstructorAnonimo();
construido1.salta();

// exporta un constructor con nombre 
var Constructor = require('./ConstructorNombre.js').Constructor;
var construido2 = new Constructor();
construido2.salta();
