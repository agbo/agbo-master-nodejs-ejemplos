'use strict';

// importamos librería events
var events = require('events');

// creamos un emisor de eventos
var myEventEmitter = new events.EventEmitter();

// le ponemos un handler (se ejecutará cada vez que alguien emita este evento)
myEventEmitter.on('suena telefono', function(quien){
    if (quien === 'madre') {
        return;
    }
    console.log('ring ring');
});

// le ponemos otro handler
myEventEmitter.on('suena telefono', function(quien){
    console.log('brr brr');
});

// emitimos el evento, deben saltar los dos handlers
myEventEmitter.emit('suena telefono', 'asfsa');