//'use strict';

function pruebaIgualdad(code) {
	// escribo expresión y su resultado
	console.log(code, eval(code));

	// cambio los == por ===
	code = code.replace('==', '===');

	// escribo la nueva expresión y su resultado
	console.log(code, eval(code), '\n');

}

// Los valores false, 0 (cero), y "" (cadena vacía) son equivalentes:
pruebaIgualdad('false == 0'); // true
pruebaIgualdad('false == ""'); // true var e = (0 == ""); // true

// Los valores null y undefined no son equivalentes con nada, 
// excepto con ellos mismos: var f = (null == false); // false
pruebaIgualdad('null == null'); // true
pruebaIgualdad('undefined == undefined'); // true var i = (undefined == null); // true

// Y por ultimo, el valor NaN no es equivalente con nada, 
// ni siquiera consigo mismo: var j = (NaN == null); // false
pruebaIgualdad('NaN == NaN'); // false