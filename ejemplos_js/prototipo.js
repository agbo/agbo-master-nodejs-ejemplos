'use strict';

// hacemos un constructor de objetos
var Persona = function(name) {
    this.name = name;
}

// creamos un objeto persona
var thomas = new Persona('thomas');

// le asignamos un metodo al objeto (solo a esta instancia)
thomas.saluda = function() {
    console.log('Hola soy ' + this.name);
};

console.log( 'thomas ' + 
	(thomas.hasOwnProperty('saluda') ? '' : 'no ') +
	'tiene método saluda'
	) ;

// lo llamamos
thomas.saluda();

// creamos otro objeto con el constructor de personas
var luis = new Persona('luis');

// veremos que no tiene el metodo saluda que pusimos a thomas
console.log( 'luis ' + 
	(luis.hasOwnProperty('saluda') ? '' : 'no ') +
	'tiene método saluda'
	) ;


// ponemos un metodo al prototipo de Persona
Persona.prototype.come = function() {
    console.log(this.name + ' come, ñam ñam');
};

// lo tienen todas las instancias

thomas.come();
luis.come();

// vamos a ver que es cada uno...
console.log('Object.getPrototypeOf(thomas)', Object.getPrototypeOf(thomas)); 
console.log('Object.getPrototypeOf(luis)', Object.getPrototypeOf(luis)); 
console.log('thomas instanceof Persona', thomas instanceof Persona);
console.log('luis instanceof Persona', luis instanceof Persona);
console.log('thomas instanceof Object', thomas instanceof Object);
console.log('luis instanceof otra cosa', luis instanceof (function Oraculo(futuro){this.futuro = futuro;}));
