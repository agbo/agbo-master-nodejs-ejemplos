"use strict";

// cargamos librería util para usar su metodo _extend
var util = require('util');

// constructor de objetos Persona
var Persona = function( nombre, apel) {
    this.nombre = nombre;
    this.apellido = apel;
};

// creamos objeto trinity
var trinity = new Persona('Trinity');

// hacemos un conjunto de propiedades (mixin) para que lo hereden
// los objetos que queramos
var matrixMixin = {
    vuela : function() {
        console.log(this.nombre + ' vuela');
    },
    esquivaBalas: function() {
        console.log(this.nombre + ' esquiva balas');
    }
};

// aplicamos el mixin (vuela y esquivaBalas) sobre todas las personas
Persona.prototype = util._extend(Persona.prototype, matrixMixin);

// creamos objeto neo
var neo = new Persona('Thomas', 'Anderson');

// todos los objetos que comparten el prototype de Persona deberían volar y esquivar balas
neo.vuela();
neo.esquivaBalas();

trinity.vuela();
trinity.esquivaBalas();
