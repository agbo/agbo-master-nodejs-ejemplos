"use strict";

// función que retorna nueva promesa
function retornaPromesa( nomfile) {
    return new Promise(function(resolve, reject){
        process.nextTick(function() {
            var err = false;
            var data = nomfile + ' - PROCESADO';
            if (err) {
                return reject(err);
            }
            return resolve(data);
        });
    });
}

// llamo la función y me devuelve la promesa
var promesa = retornaPromesa('fichero.txt');

// la uso
promesa.then( function(data) {
    console.log(data);
}).catch( function(err) {
    console.log('ERROR', err);
});
