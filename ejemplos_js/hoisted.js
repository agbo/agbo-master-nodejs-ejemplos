"use strict";

var pinto = 'my value'; 
  
function pinta() { 
  console.log('pinto', pinto); // my value 

  // esto hara hoisting de 'var pinto;' poniendolo encima del console.log
  // por tanto perderé la visibilidad de la variable pinto externa
  //var pinto = 'local value'; 
};

pinta();
