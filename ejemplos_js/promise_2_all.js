"use strict";

var fs = require('fs');

function echar( ing) {
    return new Promise(function(resolve, reject){
        process.nextTick(function() {
            //return reject(plato);
            return resolve(ing + ' echado');
        });
    });
}

var ingredientes = ['sal','pimienta','conejo','gambas'];

var promisedTexts = ingredientes.map(echar);

console.log(promisedTexts); // array de promesas en curso

// como se cuando acaban?

Promise.all(promisedTexts)
    .then(function (texts) {
        // han acabado todas
        console.log(texts);
    })
    .catch(function (reason) {
        // llegaremos aqui con el primero que falle
    });
