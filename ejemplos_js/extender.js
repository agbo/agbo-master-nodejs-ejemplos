'use strict';

// creo un objeto con notación literal
var one = {
    id : 123,
    count: 41,
    desc: 'esto es la descripción'
};

// creo otro objeto 
var two = {
    name: 'El nombre',
    tag : 'js',
    values: [1,2,3]
};

// y otro
var three = {
    count: 42,
    title: 'titulo',
    desc: null,
    values: [4,5,6]
};

// cargamos libreria 'util' del api de node
var util = require('util');

// usamos util._extend para copiar las propiedades de two en one, y devolver one 
// nota: esto modifica tambien one!
var extendido = util._extend(one, two);

// copiamos tambien las de three, por tanto extendido tendrá todas
console.log(util._extend(extendido, three));

// si queremos crean un nuevo objeto con la suma de las propiedades
// de otros, sin modificar los otros lo hacemos sobre un objeto vacio
var nuevo = util._extend({}, one, two, three);
console.log(nuevo);