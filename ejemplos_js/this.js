"use strict";


var Coche = function(name) {
    this.name = name;
    this.saluda = function() {
        console.log('Hola soy ' + this.name);
    }
};

var tesla = new Coche('tesla');


var Planta = function(name) {
    this.name = name;
    this.crece = tesla.saluda; // reutilizo el metodo
    this.crece1 = function() {
        tesla.saluda();
    }
}

// Creamos un objeto


// Lllamamos al metodo
tesla.saluda();

// Otro llama al metodo, el this de saluda ya no es tesla
setTimeout(tesla.saluda, 1000);


var planta = new Planta('flor');

//console.log(planta.name);

// reutilizamos el metodo 
// (cuidado, el objeto planta debe tener las propiedes que usaemos en los métodos)
//planta.crece();
//planta.crece1();