"use strict";

// definir una función asíncrona que devuelva una promesa en vez de un callback

function espera(options) {

    // validación
    if (options.texto === '') {
        return Promise.reject('Error de validacion!');
    }

    return new Promise(function(resolve, reject){

        setTimeout(function() {
            console.log(options.texto);
            options.texto = options.texto + '-1';
            resolve(options);
            //reject('FALLO GARRAFAL!');

        }, options.tiempo);

    });
}

// llamar a la función
var promesa = espera({ tiempo: 1000, texto: 'Smith'});


promesa = promesa.then(espera);

promesa = promesa.then(espera);

promesa = promesa.then(espera);

promesa = promesa.then(espera);

promesa.then( function(options) {
        // cambio los resultados intermedios
        options.texto = 'Jones';
        return espera(options);
    })
    .then(espera)
    .then(function(result) {
        console.log(result);
    })
    .catch( function(err){
        // si falla cualquiera recibo el error aquí
        console.log('Hubo un error:', err);
    });
