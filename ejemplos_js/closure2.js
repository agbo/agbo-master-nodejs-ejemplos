'use strict';

// hacer una función que devuelve otra
// la segunda tendrá acceso al scope de su madre

// esta vez devolveremos las funciones agrupadas en un objeto

function creaUsuario(name) {
    var surName = 'Anderson';
    return {
        login: function() {
            console.log(name + ' ha hecho login');
        },
        setName: function(newName) {
            name = newName;
        },
        logout: function() {
            console.log(name + ' ha hecho logout');
        },
        getSurname: function() {
            return surName;
        }
    };
}

// creamos el closure

var user = creaUsuario('tesla');

// usamos el closure

user.login();
user.setName('neo');
//user.logout();

// No pierden el acceso a su padre (user)!! aunque las llamemos desde otros sitios
// (como si les pasa a los objetos donde usamos this)
setTimeout(user.logout, 1000);

console.log(user.getSurname());

