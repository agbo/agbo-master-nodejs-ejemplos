"use strict";

console.log('empiezo');

// función con resultado asíncrono
// no lo devuelve con return, si no en el callback
var escribeTras2Segundos = function(texto, callBack) {
    setTimeout( function(){
        console.log(texto);
        callBack();
    }, 1000);
};


// Llamar a la funcion escribeTras2Segundos,
// le decimos que cuando termine invoque el
// callback que le pasamos como último argumento.
escribeTras2Segundos('texto1', function(){
    console.log('termino!');
});



